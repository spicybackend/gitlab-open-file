class BaseProtocol {
  static editorName() {}
  static uriPrefix() {}
  static lineJumpSuffix(lineNumber) {}

  static buildUrl(filename, lineNumber) { return `${this.uriPrefix()}${filename}${this.lineJumpSuffix(lineNumber)}` }
}

class AtomProtocol extends BaseProtocol {
  static editorName() { return "Atom"; }
  static uriPrefix() { return "atom://core/open/file?filename="; }
  static lineJumpSuffix(lineNumber) { return `&line=${lineNumber}`; }
}

class VsCodeProtocol extends BaseProtocol {
  static editorName() { return "VS Code"; }
  static uriPrefix() { return "vscode://file/"; }
  static lineJumpSuffix(lineNumber) { return `:${lineNumber}`; }
}

class RubyMineProtocol extends BaseProtocol {
  static editorName() { return "RubyMine"; }
  static uriPrefix() { return "x-mine://open?file="; }
  static lineJumpSuffix(lineNumber) { return `&line=${lineNumber}`; }
}

let editorNameToProtocolMap = {
  "Atom": AtomProtocol,
  "VS Code": VsCodeProtocol,
  "RubyMine": RubyMineProtocol
}

function protocolFor(editorName) { return editorNameToProtocolMap[editorName]; }
